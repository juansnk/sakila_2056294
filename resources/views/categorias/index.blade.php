<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Categorias: </h1>
    <table class="table table-hover">
        <tr>
            <th>
                Nombre
            </th>
            <th>
                Actualizar
            </th>
        </tr>
        @foreach($categorias as $c)
        <tr>
            <td>{{  $c->name }}</td>
            <td><a href="{{ url('categorias/edit/'.$c->category_id  )  }}"   > 
                    Actualizar
                </a>
            </td>
        </tr>
        @endforeach
    </table>
    {{ $categorias->links()   }}
</body>
</html>