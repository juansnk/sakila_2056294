<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" />
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Añadir Categoria</title>
	</head>
	<body>
		@if(session("mensaje"))
			<p class="alert-success">{{session("mensaje")}}</p>
		@endif	
		<form method="POST" action="{{ url('categorias2/store') }}" class="form-horizontal">
			@csrf
			<fieldset>
			<!-- Form Name -->
				<legend>Nueva Categoría</legend>
				<!-- Text input-->
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Nombre Categoría:</label>
					<div class="col-md-4">
						<input id="textinput" name="categoria" type="text" placeholder="" class="form-control input-md">
						<strong class="text-danger">{{$errors->first("categoria")}}</strong>
					</div>
				</div>
				<!-- Button -->
				<div class="form-group">
					<label class="col-md-4 control-label" for=""></label>
					<div class="col-md-4">
						<button type="submit" id="" name="" class="btn btn-primary">Enviar</button>
					</div>
				</div>
			</fieldset>
		</form>
	</body>
</html>