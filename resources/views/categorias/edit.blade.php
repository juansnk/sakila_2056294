<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=d, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet" >
</head>
<body>

    <!-- Si la variable de sesion "mensaje" existe, la muestra -->
    @if(session("mensaje"))
        <p class="alert-success"> {{   session("mensaje")   }}  </p>
    @endif
    

<form class="form-horizontal" action='{{ url("categorias/update/$categoria->category_id")  }}' method="post" >
<fieldset>
@csrf
<!-- Form Name -->
<legend>Nueva Categoria</legend>

<!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="categoria">Nombre:</label>  
                <div class="col-md-4">
                <input id="categoria"  value="{{  $categoria->name   }}" name="categoria" type="text" placeholder="" class="form-control input-md">
                <strong class="text-danger"> {{  $errors->first("categoria")   }}  </strong>     
                </div>
            </div>
            <!-- Button -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="singlebutton"></label>
                <div class="col-md-4">
                    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
                </div>
            </div>

        </fieldset>
    </form>
</body>
</html>