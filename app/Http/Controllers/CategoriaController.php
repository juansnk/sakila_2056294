<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{

    public function index(){

        $categorias = Categoria::paginate(5);
        return view('categorias.index')
               ->with("categorias" , $categorias); 
    }
    
    public function create(){
        return view("categorias.new");
    }


    public function store(Request $r){ 

        //Validacion
        //1. establecer las reglas de validacion para cada campo
        $reglas = [
            "categoria" => ["required", "alpha" ]
         ];

        $mensajes = [
            "required" => "Campo obligatorio",
            "alpha" => "Solo letras"
        ];    


         //2. crear el objeto validador
         $validador = Validator::make($r->all() , $reglas, $mensajes);

         //3. Validar : metodo fails
         //             retorna true(v) si la validacion falla 
         //             retorna falso en caso de que los datos sean correctos
         if ($validador->fails()){
            //codigo para cuando falla la validacion
            return redirect("categorias/create")->withErrors($validador);
         }else{
            //codigo cuando la validacion es correcta
         }

        $categoria = new Categoria();
        //trae datos desde el campo del formulario "categoria
        $categoria->name = $r->input("categoria");
        $categoria->save();
        //redireccion con datos de sesion:
        return redirect('categorias/create')->with("mensaje" , "Categoria Guardada");

    }

    public function edit($category_id){
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);

        //mostrar la vista de actualizar categotia
        //levando dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);

    }

    public function update($category_id){
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //editar sus atributos
        $categoria->name = $_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //retornar formulario de edit
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria editada");
    }

}
